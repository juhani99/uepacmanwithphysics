// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PacmanWithPhysicsGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PACMANWITHPHYSICS_API APacmanWithPhysicsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnObjectsBasedOnColor.h"

// Sets default values
ASpawnObjectsBasedOnColor::ASpawnObjectsBasedOnColor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

   

    
}

// Called when the game starts or when spawned
void ASpawnObjectsBasedOnColor::BeginPlay()
{
	Super::BeginPlay();
	

    //Texture = LoadObject<UTexture2D>(NULL, TEXT("/Game/Assets/PacmanPhysicsMap1.PacmanPhysicsMap1"), NULL, LOAD_None);

    textureSizeY = Texture->GetSizeY();
    textureSizeX = Texture->GetSizeX();

    ASpawnObjectsBasedOnColor::SpawnObjectsFromImage(Texture);
}

// Called every frame
void ASpawnObjectsBasedOnColor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpawnObjectsBasedOnColor::SpawnObjectsFromImage(UTexture2D* image)
{
    FColor* TextureData = (FColor*)image->PlatformData->Mips[0].BulkData.Lock(LOCK_READ_ONLY);
    
   

    // Loop through the pixel data and spawn objects for matching colors
    for (int Y = 0; Y < textureSizeY; Y++)
    {
        for (int X = 0; X < textureSizeX; X++)
        {
            // Get the color of the current pixel
            FColor PixelColor = TextureData[Y * textureSizeX + X];

            // Check if the color matches the color you want to spawn an object for
          
            UClass** SpawnClass = ColorClassMap.Find(PixelColor);

            if (SpawnClass)
            {
                FVector Location = FVector(X * mapScaler, Y * mapScaler, spawnZ);

                // Spawn the object
                FActorSpawnParameters SpawnParams;

                AActor* spawnedObject;

                spawnedObject = GetWorld()->SpawnActor<AActor>(*SpawnClass, Location, FRotator::ZeroRotator, SpawnParams);

                spawnedObject->AttachToActor(this,FAttachmentTransformRules::KeepRelativeTransform,"");

                UE_LOG(LogTemp, Warning, TEXT("ActorSpawned"));
            }       
        }
    }

    // Unlock the texture data
    Texture->PlatformData->Mips[0].BulkData.Unlock();
}


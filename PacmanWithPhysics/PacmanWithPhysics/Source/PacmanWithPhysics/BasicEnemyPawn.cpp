// Fill out your copyright notice in the Description page of Project Settings.


#include "BasicEnemyPawn.h"

// Sets default values
ABasicEnemyPawn::ABasicEnemyPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	rayStartPoses.Init(FVector::ZeroVector, 8); 
	rayBlocked.Init(false, 8);
	dirsBlocked.Init(false, 4);
	rayHits.Init(FHitResult::FHitResult(), 8);
}

FTransform newTransform;
APlayerCharacter* PC_ref;
UPointsManager* PM_ref;


void ABasicEnemyPawn::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	RemoveReferences();
}

// Called when the game starts or when spawned
void ABasicEnemyPawn::BeginPlay()
{
	Super::BeginPlay();

	RemoveReferences();

	PC_ref = Cast<APlayerCharacter>(GetWorld()->GetFirstPlayerController()->GetCharacter());
	PM_ref = PC_ref->PointsManager;

	if (!PM_ref)
	{
		UE_LOG(LogTemp, Error, TEXT("Cast failed"));
	}


	enemyPos = GetActorLocation();

	rayStartPoses[0] = FVector(enemyPos.X - 152, enemyPos.Y, enemyPos.Z + 152); //top left up
	rayStartPoses[1] = FVector(enemyPos.X + 152, enemyPos.Y, enemyPos.Z + 152); //Top right up
	rayStartPoses[2] = FVector(enemyPos.X + 152, enemyPos.Y, enemyPos.Z + 152); //Top right right
	rayStartPoses[3] = FVector(enemyPos.X + 152, enemyPos.Y, enemyPos.Z - 152); //bottom right right
	rayStartPoses[4] = FVector(enemyPos.X + 152, enemyPos.Y, enemyPos.Z - 152); //bottom right down
	rayStartPoses[5] = FVector(enemyPos.X - 152, enemyPos.Y, enemyPos.Z - 152); //bottom left down
	rayStartPoses[6] = FVector(enemyPos.X - 152, enemyPos.Y, enemyPos.Z - 152); //bottom left left
	rayStartPoses[7] = FVector(enemyPos.X - 152, enemyPos.Y, enemyPos.Z + 152); //top left left

	for (int i = 0; i < raycasterAmount; i++)
	{
		rayEndPositions.Add(rayStartPoses[i] + rayDirections[i] * (raycasterRange/2));
		DetectWalls(i); //Does the linetracing

		if (rayHits[i].GetActor() != NULL)
		{
			if (rayHits[i].GetActor()->ActorHasTag(TEXT("Wall")))
			{
				rayBlocked[i] = true;
			}
			else
			{
				rayBlocked[i] = false;
			}
		}
		else
		{
			rayBlocked[i] = false;
		}

	}

	dirsBlocked = UpdateDirsBlocked(rayBlocked);
	RandomizeDir(movementDirs);
}

TArray<bool> oldDirsBlocked;


void ABasicEnemyPawn::RemoveReferences()
{
	if (PM_ref != nullptr)
	{
		// Remove all references to PointsManager
		for (TObjectIterator<UObject> It; It; ++It)
		{
			UObject* Object = *It;

			// Check if the object is a blueprint
			if (Object->IsA<UBlueprint>())
			{
				UBlueprint* Blueprint = Cast<UBlueprint>(Object);

				// Check if the blueprint is of the PlayerCharacterBP class
				if (Blueprint->GeneratedClass->IsChildOf(APlayerCharacter::StaticClass()))
				{
					// Cast to the PlayerCharacterBP and remove reference to PointsManager
					APlayerCharacter* PlayerCharacterBP = Cast<APlayerCharacter>(Blueprint->GeneratedClass->GetDefaultObject());
					if (PlayerCharacterBP && PlayerCharacterBP->PointsManager == PM_ref)
					{
						PlayerCharacterBP->PointsManager = nullptr;
					}
				}
			}
		}

		// Destroy the PointsManager object
		PM_ref->RemoveFromRoot();
		//PM_ref->ConditionalBeginDestroy();
		PM_ref = nullptr;
	}
}

// Called every frame
void ABasicEnemyPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//paused = PM_ref->GetPaused();

	oldDirsBlocked = dirsBlocked;

	enemyPos = GetActorLocation();
	rayStartPoses[0] = FVector(enemyPos.X - 152, enemyPos.Y, enemyPos.Z + 152); //top left up
	rayStartPoses[1] = FVector(enemyPos.X + 152, enemyPos.Y, enemyPos.Z + 152); //Top right up
	rayStartPoses[2] = FVector(enemyPos.X + 152, enemyPos.Y, enemyPos.Z + 152); //Top right right
	rayStartPoses[3] = FVector(enemyPos.X + 152, enemyPos.Y, enemyPos.Z - 152); //bottom right right
	rayStartPoses[4] = FVector(enemyPos.X + 152, enemyPos.Y, enemyPos.Z - 152); //bottom right down
	rayStartPoses[5] = FVector(enemyPos.X - 152, enemyPos.Y, enemyPos.Z - 152); //bottom left down
	rayStartPoses[6] = FVector(enemyPos.X - 152, enemyPos.Y, enemyPos.Z - 152); //bottom left left
	rayStartPoses[7] = FVector(enemyPos.X - 152, enemyPos.Y, enemyPos.Z + 152); //top left left

	for (int i = 0; i < raycasterAmount; i++)
	{
		rayEndPositions[i] = rayStartPoses[i] + rayDirections[i] * (raycasterRange/2);
		DetectWalls(i); //Does the linetracing

		if (rayHits[i].GetActor() != NULL)
		{
			if (rayHits[i].GetActor()->ActorHasTag(TEXT("Wall")))
			{
				rayBlocked[i] = true;
			}
			else
			{
				rayBlocked[i] = false;
			}
		}
		else
		{
			rayBlocked[i] = false;
		}
	}

	dirsBlocked = UpdateDirsBlocked(rayBlocked);

	if (oldDirsBlocked == dirsBlocked)
	{
		//Keep moving to current direction
		if (!PM_ref->GetPaused())
		{
			AddActorWorldTransform(newTransform, true);
		}
	}
	else
	{
		//Give new dir
		movementDir = RandomizeDir(movementDirs);


		newTransform = FTransform(FRotator::ZeroRotator, movementDir * movementSpeed, FVector::ZeroVector);
		
	}
}

TArray <bool> ABasicEnemyPawn::UpdateDirsBlocked(TArray<bool> rayBlocked_)
{
	TArray <bool> newDirs;
	newDirs.Init(true, rayBlocked_.Num() / 2);


	if (!rayBlocked_[0] && !rayBlocked_[1]) //Checks up
	{
		newDirs[0] = false;
	}

	if (!rayBlocked_[2] && !rayBlocked_[3]) //Checks right
	{
		newDirs[1] = false;
	}

	if (!rayBlocked_[4] && !rayBlocked_[5]) //Checks down
	{
		newDirs[2] = false;
	}

	if (!rayBlocked_[6] && !rayBlocked_[7]) //Checks left
	{
		newDirs[3] = false;
	}

	return newDirs;
}

FVector ABasicEnemyPawn::RandomizeDir(TArray<FVector> movementDirs_)
{
	TArray<FVector> possibleDirs;

	for (int i = 0; i < movementDirs_.Num(); i++)
	{
		if (!dirsBlocked[i])
		{
			possibleDirs.Add(movementDirs_[i]);
		}
	}
	int randomDir = FMath::RandRange(0, possibleDirs.Num()-1);

	return possibleDirs[randomDir];
}

// Called to bind functionality to input
void ABasicEnemyPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}


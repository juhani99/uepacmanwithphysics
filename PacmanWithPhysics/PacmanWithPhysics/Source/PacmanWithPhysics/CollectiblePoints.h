// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CollectiblePoints.generated.h"

UCLASS()
class PACMANWITHPHYSICS_API ACollectiblePoints : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACollectiblePoints();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Points")
	int pointsToGive;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

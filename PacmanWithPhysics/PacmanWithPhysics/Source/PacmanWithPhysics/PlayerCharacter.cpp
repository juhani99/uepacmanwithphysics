// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"

// Sets default values
APlayerCharacter::APlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();

	RemoveReferences();

	PointsManager = UPointsManager::GetInstance();
	playerYPos = GetActorLocation().Y;
}

void APlayerCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	
	RemoveReferences();
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//currentVelocity = Get

	APlayerCharacter::BaseMovement();
	APlayerCharacter::BaseFalling();
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void APlayerCharacter::RemoveReferences()
{
	if (PointsManager != nullptr)
	{
		// Remove all references to PointsManager
		for (TObjectIterator<UObject> It; It; ++It)
		{
			UObject* Object = *It;

			// Check if the object is a blueprint
			if (Object->IsA<UBlueprint>())
			{
				UBlueprint* Blueprint = Cast<UBlueprint>(Object);

				// Check if the blueprint is of the PlayerCharacterBP class
				if (Blueprint->GeneratedClass->IsChildOf(APlayerCharacter::StaticClass()))
				{
					// Cast to the PlayerCharacterBP and remove reference to PointsManager
					APlayerCharacter* PlayerCharacterBP = Cast<APlayerCharacter>(Blueprint->GeneratedClass->GetDefaultObject());
					if (PlayerCharacterBP && PlayerCharacterBP->PointsManager == PointsManager)
					{
						PlayerCharacterBP->PointsManager = nullptr;
					}
				}
			}
		}

		// Destroy the PointsManager object
		PointsManager->RemoveFromRoot();
		PointsManager->ConditionalBeginDestroy();
		PointsManager = nullptr;
	}
}


void APlayerCharacter::BaseMovement()
{
	switch (moveState)
	{
	case moveState::walkState:
		currentMovementSpeed = normalMovementSpeed;
		break;
	case moveState::sneakState:
		currentMovementSpeed = sneakSpeed;
		break;
	case moveState::runState:
		currentMovementSpeed = runSpeed;
		break;
	}

	if (GetActorLocation().Y != playerYPos)
	{
		SetActorLocation(FVector(GetActorLocation().X, playerYPos, GetActorLocation().Z));
	}
}

void APlayerCharacter::SavePoints(FString _playerName, int _playerScore)
{
	PointsManager->AddBestScores(_playerName, _playerScore);
	PointsManager->SaveBestScores();
	bestScores = PointsManager->GetBestScores();
}

void APlayerCharacter::BaseFalling()
{
	FVector currentVelocity = GetCharacterMovement()->Velocity;

	if (currentVelocity.Z > 0.0f)
	{
		jumpState = jumpState::risingState;
	}
	else if (currentVelocity.Z < 0.0f && jumpState != jumpState::fastFallingState)
	{
		jumpState = jumpState::normalFallingState;
	}
	else if (currentVelocity.Z == 0.0f)
	{
		jumpState = jumpState::groundedState;
		GetCharacterMovement()->GravityScale = 1;
	}
}


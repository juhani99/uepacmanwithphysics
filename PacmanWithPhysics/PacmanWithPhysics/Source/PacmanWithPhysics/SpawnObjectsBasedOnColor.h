// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpawnObjectsBasedOnColor.generated.h"

UCLASS()
class PACMANWITHPHYSICS_API ASpawnObjectsBasedOnColor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnObjectsBasedOnColor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, Category = "Texture", BlueprintReadWrite)
		UTexture2D* Texture;

	UPROPERTY(VisibleAnywhere, Category = "Texture", BlueprintReadOnly)
		int textureSizeY;

	UPROPERTY(VisibleAnywhere, Category = "Texture", BlueprintReadOnly)
		int textureSizeX;
	
	UPROPERTY(EditAnywhere, Category = "Texture", BlueprintReadWrite)
		float mapScaler;

	UPROPERTY(EditAnywhere, Category = "Texture", BlueprintReadWrite)
		TMap<FColor, UClass*> ColorClassMap;

	UPROPERTY(EditAnywhere, Category = "Texture", BlueprintReadWrite)
		float spawnZ;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(Category = "Texture", BlueprintCallable)
		void SpawnObjectsFromImage(UTexture2D* image);

};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpawnCollectibles.generated.h"

UCLASS()
class PACMANWITHPHYSICS_API ASpawnCollectibles : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnCollectibles();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	UPROPERTY(EditAnywhere, Category = "Corners", BlueprintReadWrite)
		FVector topLeftCorner = FVector(-39450, -20, -450);

	UPROPERTY(EditAnywhere, Category = "Corners", BlueprintReadWrite)
		FVector topRightCorner = FVector(-450, -20, -450);

	UPROPERTY(EditAnywhere, Category = "Corners", BlueprintReadWrite)
		FVector bottomLeftCorner = FVector(-39450, -20, -39450);

	UPROPERTY(EditAnywhere, Category = "Corners", BlueprintReadWrite)
		FVector bottomRightCorner = FVector(-450, -20, -39450);

	UPROPERTY(EditAnywhere, Category = "Collectibles", BlueprintReadWrite)
		float distanceBetweenCollectibles = 460;

	UPROPERTY(EditAnywhere, Category = "Collectibles", BlueprintReadWrite)
		TArray <FVector> CollectiblePositions;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, Category = "Collectibles", BlueprintReadWrite)
		int totalCollectibles;

	UPROPERTY(EditAnywhere, Category = "Collectibles", BlueprintReadWrite)
		int existingCollectibles;

	UFUNCTION(BlueprintImplementableEvent, Category = "Collectibles", BlueprintCallable)
		void SpawnItems();
	

	UFUNCTION(BlueprintImplementableEvent, Category = "DeleteWalls", BlueprintCallable)
		void DeleteWalls();

};

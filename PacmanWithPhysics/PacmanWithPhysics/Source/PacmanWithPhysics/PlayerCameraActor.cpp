// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCameraActor.h"

// Sets default values
APlayerCameraActor::APlayerCameraActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APlayerCameraActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlayerCameraActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

    // Get the camera boom and camera components
    USpringArmComponent* CameraBoom = FindComponentByClass<USpringArmComponent>();
    UCameraComponent* Camera = FindComponentByClass<UCameraComponent>();

    // Set the camera's location based on the target's location
    FVector targetLocation = TargetActor->GetActorLocation();
    FVector cameraBoomLocation = FVector(targetLocation.X, cameraDistance, targetLocation.Z);

    CameraBoom->SetWorldLocation(cameraBoomLocation);
    //CameraBoom->AddLocalOffset(FVector(0, CameraDistance, 0));

    // Clamp the camera's location within the bounds
    FVector cameraLocation = CameraBoom->GetComponentLocation();
    cameraLocation.X = FMath::Clamp(cameraLocation.X, minBounds.X, maxBounds.X);
    //cameraLocation.Y = FMath::Clamp(cameraLocation.Y, minBounds.Y, maxBounds.Y);
    cameraLocation.Z = FMath::Clamp(cameraLocation.Z, minBounds.Z, maxBounds.Z);
    CameraBoom->SetWorldLocation(cameraLocation);

}


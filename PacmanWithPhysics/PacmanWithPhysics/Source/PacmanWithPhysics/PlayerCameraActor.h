// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PlayerCameraActor.generated.h"


UCLASS()
class PACMANWITHPHYSICS_API APlayerCameraActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlayerCameraActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Set this value to the distance you want the camera to be from the target
	UPROPERTY(EditAnywhere, Category = "CameraConfig", BlueprintReadWrite)
		float cameraDistance = 10000.0f;

	// Set these values to your desired bounds
	UPROPERTY(EditAnywhere, Category = "CameraConfig", BlueprintReadWrite)
		FVector minBounds = FVector(-30030, cameraDistance,-35950);
	UPROPERTY(EditAnywhere, Category = "CameraConfig", BlueprintReadWrite)
		FVector maxBounds = FVector(-9700, cameraDistance, -3900);



public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, Category = "CameraConfig", BlueprintReadWrite)
		AActor* TargetActor;

};

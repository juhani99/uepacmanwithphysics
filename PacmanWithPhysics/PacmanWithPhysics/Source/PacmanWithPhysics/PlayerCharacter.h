// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Engine/Engine.h"
#include "PointsManager.h"
#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"

UENUM()
enum jumpState
{
	groundedState UMETA(DisplayName = "groundedState"),
	risingState UMETA(DisplayName = "risingState"),
	normalFallingState UMETA(DisplayName = "normalFallingState"),
	fastFallingState UMETA(DisplayName = "fastFallingState"),
};

UENUM()
enum moveState
{
	walkState UMETA(DisplayName = "walkState"),
	sneakState UMETA(DisplayName = "sneakState"),
	runState UMETA(DisplayName = "runState"),
};


UCLASS()
class PACMANWITHPHYSICS_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UPROPERTY(EditAnywhere, Category = "Movement", BlueprintReadWrite)
		float normalMovementSpeed;

	UPROPERTY(EditAnywhere, Category = "Movement", BlueprintReadWrite)
		float sneakSpeed;

	UPROPERTY(EditAnywhere, Category = "Movement", BlueprintReadWrite)
		float runSpeed;

	UPROPERTY(VisibleAnywhere, Category = "Movement", BlueprintReadWrite)
		float currentMovementSpeed;

	UPROPERTY(VisibleAnywhere, Category = "Movement", BlueprintReadWrite)
		float playerYPos;


	UPROPERTY(VisibleAnywhere, Category = "Movement", BlueprintReadWrite)
		TEnumAsByte<moveState> moveState;

	UPROPERTY(VisibleAnywhere, Category = "Points", BlueprintReadWrite)
		int playerScore;

	UPROPERTY(VisibleAnywhere, Category = "Points", BlueprintReadWrite)
		FString playerName;


	UPROPERTY(EditAnywhere, Category = "Jump", BlueprintReadWrite)
		float jumpFullForce;

	UPROPERTY(EditAnywhere, Category = "Jump", BlueprintReadWrite)
		float jumpShortForce;

	UPROPERTY(EditAnywhere, Category = "Jump", BlueprintReadWrite)
		float jumpTime;


	UPROPERTY(VisibleAnywhere, Category = "Jump", BlueprintReadWrite)
		TEnumAsByte<jumpState> jumpState;

	UPROPERTY(EditAnywhere, Category = "Jump", BlueprintReadWrite)
		float fallMultiplier;

	UFUNCTION(Category = "Movement", BlueprintCallable)
		void BaseMovement();
	
	UFUNCTION(Category = "Jump", BlueprintCallable)
		void BaseFalling();


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, Category = "Jump", BlueprintReadWrite)
		bool lineTracer;

	UPROPERTY(EditAnywhere, Category = "Points", BlueprintReadWrite)
	UPointsManager* PointsManager;


	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Points")
	TArray <FScoreEntry> bestScores;

	UFUNCTION(Category = "Points", BlueprintCallable)
		void SavePoints(FString _playerName, int _playerScore);

	void RemoveReferences();


	UFUNCTION(BlueprintImplementableEvent, Category = "Movement", BlueprintCallable)
		void EnableKeys();

};

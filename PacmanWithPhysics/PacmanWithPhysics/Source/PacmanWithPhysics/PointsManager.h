// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "PointsManager.generated.h"


USTRUCT(BlueprintType)
struct FScoreEntry
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	FString playerName;

	UPROPERTY(BlueprintReadWrite)
	int scoreValue;
};

UCLASS()
class PACMANWITHPHYSICS_API UPointsManager : public UObject
{
	GENERATED_BODY()

public:	

	static UPointsManager* GetInstance();

	virtual void BeginDestroy() override;

	//UPointsManager();
	//UPointsManager(const UPointsManager&) = delete;
	//UPointsManager& operator=(const UPointsManager&) = delete;

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Points")
		int points;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Points")
		FString newPlayerName;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Points")
		int highScore;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Points")
		TArray <FScoreEntry> bestScores;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Points")
		int collectiblesPicked;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Points")
		int newTotalCollectibles;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Paused")
		bool paused = false;


public:	

	UFUNCTION(Category = "Points", BlueprintCallable)
		void AddPoints(int pointAmount);

	UFUNCTION(Category = "Points", BlueprintCallable)
		int GetPoints() const { return points; }

	UFUNCTION(Category = "Points", BlueprintCallable)
		FString GetName() const { return newPlayerName; }

	UFUNCTION(Category = "Points", BlueprintCallable)
		int GetHighScore() const { return highScore; }

	UFUNCTION(Category = "Paused", BlueprintCallable)
		bool GetPaused() const { return paused; }

	UFUNCTION(Category = "paused", BlueprintCallable)
		void SetPaused(bool value);

	UFUNCTION(Category = "Points", BlueprintCallable)
		TArray <FScoreEntry> GetBestScores() const { return bestScores; }

	UFUNCTION(Category = "Points", BlueprintCallable)
		void AddBestScores(const FString& playerName, int _points);

	UFUNCTION(Category = "Points", BlueprintCallable)
		void SaveBestScores();

	UFUNCTION(Category = "Points", BlueprintCallable)
		void LoadBestScores();

	UFUNCTION(Category = "Points", BlueprintCallable)
		void SetPlayerName(FString _playerName);

	UFUNCTION(Category = "Points", BlueprintCallable)
		void SetNewTotalCollectibles(int amount);

	UFUNCTION(Category = "Points", BlueprintCallable)
		void GameEndCondition(bool win);

private:

	static TUniquePtr<UPointsManager> Instance;
};

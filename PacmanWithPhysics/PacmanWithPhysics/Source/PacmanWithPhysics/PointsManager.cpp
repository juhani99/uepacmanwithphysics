// Fill out your copyright notice in the Description page of Project Settings.

#include "PointsManager.h"
#include "PlayerCharacter.h"

TUniquePtr <UPointsManager> UPointsManager::Instance;

void UPointsManager::BeginDestroy()
{
    Super::BeginDestroy();
    Instance.Reset();
}

void UPointsManager::AddPoints(int pointsAmount)
{
	points = points + pointsAmount;
    collectiblesPicked++;

    if (collectiblesPicked >= newTotalCollectibles && newTotalCollectibles > 0)
    {
        //Player wins
        GameEndCondition(true);
    }
}

UPointsManager* UPointsManager::GetInstance()
{
    if (Instance == nullptr)
    {
        // Create a new instance if one does not already exist
        Instance = TUniquePtr<UPointsManager>(NewObject<UPointsManager>(GetTransientPackage(), FName(TEXT("PointsManager"))));
        check(Instance);        
    }

    return Instance.Get();
}

void UPointsManager::SetPlayerName(FString _playerName)
{
    newPlayerName = _playerName;
}


void UPointsManager::AddBestScores(const FString&playerName ,int _points)
{
    FScoreEntry newScore;
    newScore.playerName = playerName;
    newScore.scoreValue = _points;

    // Add the new score to the array
    bestScores.Add(newScore);

    // Sort the array in descending order by score value
    bestScores.Sort([](const FScoreEntry& A, const FScoreEntry& B)
        {
            return A.scoreValue > B.scoreValue;
        });

    // Limit the array to the first 5 entries
    if (bestScores.Num() > 5)
    {
        bestScores.RemoveAt(5, bestScores.Num() - 5);
    }
}

void UPointsManager::SaveBestScores()
{
    if (bestScores.Num() > 0)
    {
        FString SaveDir = FPaths::ProjectDir() + "/Saved/Scores/";
        FString SavePath = SaveDir + "BestScores.txt";

        IPlatformFile& PlatformFile = FPlatformFileManager::Get().GetPlatformFile();
        if (!PlatformFile.DirectoryExists(*SaveDir))
        {
            PlatformFile.CreateDirectory(*SaveDir);
        }

        FString saveString;

        for (const auto& score : bestScores)
        {
            saveString += FString::Printf(TEXT("%s,%d\n"), *score.playerName, score.scoreValue);
        }

        bool bSuccess = FFileHelper::SaveStringToFile(saveString, *SavePath);

        if (!bSuccess)
        {
            UE_LOG(LogTemp, Error, TEXT("Failed to save high scores to file"));
        }
        else
        {
            UE_LOG(LogTemp, Error, TEXT("saved high scores to file"));
        }
    }
}

void UPointsManager::LoadBestScores()
{
    FString SaveDir = FPaths::ProjectDir() + "/Saved/Scores/";
    FString SavePath = SaveDir + "BestScores.txt";


    FString InputString;
    bool bSuccess = FFileHelper::LoadFileToString(InputString, *SavePath);

    if (!bSuccess)
    {
        UE_LOG(LogTemp, Error, TEXT("Failed to read high scores from file"));
        return;
    }
    else
    {
        UE_LOG(LogTemp, Error, TEXT("loaded high scores from file"));
    }

    bestScores.Empty();

    TArray<FString> Lines;
    InputString.ParseIntoArrayLines(Lines);
    for (const auto& Line : Lines)
    {
        TArray<FString> LineSplit;
        Line.ParseIntoArray(LineSplit, TEXT(","), true);
        if (LineSplit.Num() == 2)
        {
            FString Name = LineSplit[0];
            int Score = FCString::Atoi(*LineSplit[1]);

            FScoreEntry ScoreEntry;
            ScoreEntry.playerName = Name;
            ScoreEntry.scoreValue = Score;
            bestScores.Add(ScoreEntry);
        }
    }  
}

void UPointsManager::SetNewTotalCollectibles(int amount)
{
    newTotalCollectibles = amount;
}

void UPointsManager::SetPaused(bool value)
{
    paused = value;
}

void UPointsManager::GameEndCondition(bool win)
{
    //UObject* Object;
    //UBlueprint* Blueprint = Cast<UBlueprint>(Object);

    //APlayerCharacter* PlayerCharacterBP = Cast<APlayerCharacter>(Blueprint->GeneratedClass->GetDefaultObject());

    paused = true;

    switch (win)
    {
    case true:

        //Player wins

        UE_LOG(LogTemp, Error, TEXT("You win"));

        break;

    case false:

        //Player loses

        UE_LOG(LogTemp, Error, TEXT("You lose"));

        break;
    }
}
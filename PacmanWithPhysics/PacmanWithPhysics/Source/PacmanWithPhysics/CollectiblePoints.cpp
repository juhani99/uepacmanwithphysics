// Fill out your copyright notice in the Description page of Project Settings.


#include "CollectiblePoints.h"

// Sets default values
ACollectiblePoints::ACollectiblePoints()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACollectiblePoints::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACollectiblePoints::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnCollectibles.h"

// Sets default values
ASpawnCollectibles::ASpawnCollectibles()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpawnCollectibles::BeginPlay()
{
	Super::BeginPlay();
	
	

	for (int i = 0; i < FMath::Floor(FMath::Abs(( - 39450.0f - 450.0f) / distanceBetweenCollectibles)); i++)
	{
		for (int j = 0; j < FMath::Floor(FMath::Abs(( -39450.0f - 450.0f) / distanceBetweenCollectibles)); j++)
		{
			CollectiblePositions.Add(FVector(bottomLeftCorner.X + i * 460, 130, bottomLeftCorner.Z + j * 460));
		}
	}	

	SpawnItems();
}

// Called every frame
void ASpawnCollectibles::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


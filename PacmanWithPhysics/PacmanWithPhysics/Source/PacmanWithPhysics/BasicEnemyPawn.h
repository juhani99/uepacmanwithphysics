// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PointsManager.h"
#include "PlayerCharacter.h"
#include "BasicEnemyPawn.generated.h"

UCLASS()
class PACMANWITHPHYSICS_API ABasicEnemyPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ABasicEnemyPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rays")
		int raycasterAmount = 8;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rays")
		float raycasterRange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float movementSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rays")
		TArray<bool> rayBlocked;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rays")
		TArray<bool> dirsBlocked;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rays")
		FVector enemyPos;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rays")
		TArray <FVector> rayStartPoses;

	UPROPERTY(EditAnywhere , BlueprintReadWrite, Category = "Rays")
		TArray<FHitResult> rayHits;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rays")
		TArray<FVector> rayDirections = { FVector(0, 0, 1), FVector(0, 0, 1), 
		FVector(1, 0, 0), FVector(1, 0, 0), 
		FVector(0, 0, -1), FVector(0, 0, -1), 
		FVector(-1, 0, 0), FVector(-1, 0, 0) };

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rays")
		TArray<FVector> rayEndPositions;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement")
		TArray<FVector> movementDirs = { FVector(0,0,1), FVector(1,0,0), FVector(0,0,-1), FVector(-1,0,0) };

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement")
		FVector movementDir;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement")
		bool paused;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintImplementableEvent, Category = "Rays")
		void DetectWalls(int currentForLoopIteration);

	UFUNCTION(BlueprintCallable, Category = "Rays")
		TArray <bool> UpdateDirsBlocked(TArray<bool> rayBlocked_);

	UFUNCTION(BlueprintCallable, Category = "PlayerProjectile")
		FVector RandomizeDir(TArray<FVector> allDirections);

	UFUNCTION(BlueprintCallable, Category = "PlayerProjectile")
		void RemoveReferences();


};

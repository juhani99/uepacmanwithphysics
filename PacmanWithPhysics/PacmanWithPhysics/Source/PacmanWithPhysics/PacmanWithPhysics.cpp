// Copyright Epic Games, Inc. All Rights Reserved.

#include "PacmanWithPhysics.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, PacmanWithPhysics, "PacmanWithPhysics" );
